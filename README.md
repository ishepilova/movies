### Movies app ###

An app to display, sort and add movies list

In order to launch the app, run 

```
#!javascript

git clone https://ishepilova@bitbucket.org/ishepilova/movies.git
npm install
bower install
gulp inject
gulp
```