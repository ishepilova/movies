(function () {
    'use strict';

    angular
        .module('movies.directives.moviesView', [])
        .directive('moviesView', function () {
            return {
                replace: true,
                templateUrl: 'app/partials/view.html'
            };
        });

})();