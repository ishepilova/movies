(function () {
    'use strict';

    angular
        .module('movies.directives.header', [])
        .directive('myHeader', function () {
            return {
                replace: false,
                template: '<div class="page-header"><h3>Movies to Watch</h3></div>'
            };
        });

})();