(function () {
    'use strict';

    angular
        .module('movies.directives', [
            'movies.directives.header',
            'movies.directives.moviesView'
        ]);

})();