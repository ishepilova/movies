angular.module('movies')

.config(function ($routeProvider, $locationProvider) {

    $locationProvider.html5Mode(true);

    $routeProvider
        .when('/', {
            templateUrl: 'app/partials/view.html',
            controller: 'MoviesController'
        })
        .otherwise({
            redirectTo: '/'
        });

});