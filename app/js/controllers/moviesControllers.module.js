(function() {
  'use strict';

  angular
    .module('movies.controllers', [
      'movies.controllers.main',
      'movies.controllers.route'
    ]);

})();