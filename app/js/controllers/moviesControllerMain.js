(function () {
    'use strict';

    angular
        .module('movies.controllers.main', [])
        .controller('MoviesController', moviesController);

    function moviesController(moviesDataService) {
        var $ctrl = this;

        //copying service data to work with a copy
        $ctrl.movies = angular.copy(moviesDataService.data);

        $ctrl.sortOptions = [
            {value: "selectSorting", label: "Sort By"},
            {value: "name", asc: true, label: "Movie name A - Z"},
            {value: "name", asc: false, label: "Movie name Z - A"},
            {value: "director", asc: true, label: "Movie director A - Z"},
            {value: "director", asc: false, label: "Movie director Z - A"},
            {value: "year", asc: true, label: "Year ascending"},
            {value: "year", asc: false, label: "Year descending"}
        ];

        $ctrl.sortBy = $ctrl.sortOptions[0];

        //function which resorts our Movies array when SortBy changed
        $ctrl.sortingChanged = function (param) {
            console.log(moviesDataService.data);
            if (param.value !== "selectSorting") {
                $ctrl.movies = $ctrl.movies.sort($ctrl.resort(param.value));
                if (!param.asc) {
                    $ctrl.movies.reverse();
                }
            } else $ctrl.movies = angular.copy(moviesDataService.data);
            return $ctrl.movies;
        };

        //function for sorting of objects array by object property
        $ctrl.resort = function (property) {
            var sortOrder = 1;
            if (property[0] === "-") {
                sortOrder = -1;
                property = property.substr(1);
            }
            return function (a, b) {
                var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
                return result * sortOrder;
            }
        };

        $ctrl.modalShown = false;

        $ctrl.hideModal = function() {
            $ctrl.modalShown = false;
        };

        $ctrl.showModal = function() {
            $ctrl.modalShown = true;
        };

        $ctrl.addNewMovie = function(newMovie) {
            $ctrl.movies.push(newMovie);
            console.log($ctrl.movies);
            $ctrl.hideModal();
        };

    }

})();