(function () {
    'use strict';

    angular
        .module('movies.services.moviesDataService', [])
        .factory('moviesDataService', [moviesDataService]);

    function moviesDataService(){
        var movies = {};

        //It should be a $http.get promise
        movies.data = [
            {
                name: 'The Shawshank Redemption',
                director: 'Frank Darabont',
                year: 1994,
                imageURL: 'https://m.media-amazon.com/images/I/51zUbui+gbL.jpg'
            },
            {
                name: 'The Godfather',
                director: 'Francis Ford Coppola',
                year: 1972,
                imageURL: 'https://m.media-amazon.com/images/M/MV5BM2MyNjYxNmUtYTAwNi00MTYxLWJmNWYtYzZlODY3ZTk3OTFlXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_.jpg'
            },
            {
                name: 'The Lord of the Rings: The Fellowship of the Ring',
                director: 'Peter Jackson',
                year: 2001,
                imageURL: 'https://images-na.ssl-images-amazon.com/images/M/MV5BN2EyZjM3NzUtNWUzMi00MTgxLWI0NTctMzY4M2VlOTdjZWRiXkEyXkFqcGdeQXVyNDUzOTQ5MjY@._V1_SY999_CR0,0,673,999_AL_.jpg'
            },
            {
                name: 'Fight Club',
                director: 'David Fincher',
                year: 1999,
                imageURL: 'https://m.media-amazon.com/images/M/MV5BNDIzNDU0YzEtYzE5Ni00ZjlkLTk5ZjgtNjM3NWE4YzA3Nzk3XkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_.jpg'
            },
            {
                name: 'Forrest Gump',
                director: 'Robert Zemeckis',
                year: 1994,
                imageURL: 'https://m.media-amazon.com/images/M/MV5BNWIwODRlZTUtY2U3ZS00Yzg1LWJhNzYtMmZiYmEyNmU1NjMzXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_.jpg'
            }
        ];

        return movies;
    }

})();
