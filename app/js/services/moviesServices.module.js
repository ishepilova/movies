(function() {
  'use strict';

  angular
    .module('movies.services', [
      'movies.services.moviesDataService'
    ]);

})();