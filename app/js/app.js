(function () {
    'use strict';

    angular
        .module('movies', [
            'ngRoute',
            'movies.directives',
            'movies.controllers',
            'movies.services'
        ]);
})();